package ch.gmtech.seminar.report;

import ch.gmtech.seminar.Seminar;
import ch.gmtech.seminar.Student;

public class SeminarTextReportStrategy extends AbstractSeminarReportStrategy {

	@Override
	public String reportFor(Seminar aSeminar) {
		StringBuilder result = new StringBuilder()
			.append("course: " + aSeminar.getName() + " - " + aSeminar.getNumber() + "\n")
			.append("description: " + aSeminar.getDescription() + "\n")
			.append( "location: " + aSeminar.getLocation() + "\n")
			.append("seats left: " + aSeminar.getSeatsLeft() + "\n")
			.append("students:\n");
		for (Student each : aSeminar.getStudentList()) {
			result.append("\t" + each.getName() + " " + each.getSurname());
		}
		return result.toString();
	}

}
