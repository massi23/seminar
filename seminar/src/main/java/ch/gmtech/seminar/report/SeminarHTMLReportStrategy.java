package ch.gmtech.seminar.report;

import ch.gmtech.seminar.Seminar;
import ch.gmtech.seminar.Student;

public class SeminarHTMLReportStrategy extends AbstractSeminarReportStrategy{

	@Override
	public String reportFor(Seminar aSeminar) {
		StringBuilder result = new StringBuilder()
			.append("<html>")
			.append("<head>")
			.append("     <title>nome corso</title>")
			.append("</head> ")
			.append("<body>")
			.append("    <div>" + aSeminar.getName() + "</div>")
			.append("    <ul>")
			.append("          <li>" + aSeminar.getDescription() + "</li>")
			.append("          <li>" + aSeminar.getLocation() + "</li>")
			.append("          <li>" + aSeminar.getSeatsLeft() + "</li>")
			.append("    </ul>")
			.append("    <div>partecipanti:</div>")
			.append("    <ul>");
		for (Student each : aSeminar.getStudentList()) {
			result.append("          <li>" + each.getName() + " " + each.getSurname() + "</li>");
		}
		
		result.append("    </ul>")
			.append("</body>")
			.append("</html>");
		return result.toString();
	}

}
