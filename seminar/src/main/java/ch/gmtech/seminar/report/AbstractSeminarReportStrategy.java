package ch.gmtech.seminar.report;

import ch.gmtech.seminar.Seminar;

public abstract class AbstractSeminarReportStrategy {

	public abstract String reportFor(Seminar aSeminar);

}
