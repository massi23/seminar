package ch.gmtech.seminar.report;

import ch.gmtech.seminar.Seminar;
import ch.gmtech.seminar.Student;

public class SeminarCSVReportStrategy extends AbstractSeminarReportStrategy {

	private final String _lineSeparator;

	public SeminarCSVReportStrategy() {
		_lineSeparator = System.getProperty("line.separator");
	}
	
	@Override
	public String reportFor(Seminar aSeminar) {
		StringBuilder result = new StringBuilder();
		result.append(csvLine(String.valueOf(aSeminar.getNumber()), aSeminar.getName(), aSeminar.getDescription(), aSeminar.getLocation(), String.valueOf(aSeminar.getSeatsLeft())));
		result.append(_lineSeparator);
		for (Student each : aSeminar.getStudentList()) {
			result.append(csvLine(each.getName(), each.getSurname()));
			result.append(_lineSeparator);
		}
		return result.toString();
	}

	private String csvLine(String ... data) {
		StringBuilder result = new StringBuilder();
		for (String each : data) {
			result.append("\"" + each + "\";");
		}
		return result.toString();
	}

}
