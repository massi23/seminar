package ch.gmtech.seminar;

import java.util.ArrayList;
import java.util.List;

import ch.gmtech.seminar.report.AbstractSeminarReportStrategy;
import ch.gmtech.seminar.report.SeminarTextReportStrategy;

public class Seminar {
	
	private final String _name;
	private final int _number;
	private final int _seatsLeft;
	private final String _description;
	private final String _location;
	private final List<Student> _enrollmentList;

	public Seminar(String name, int number, String description, String localtion, int seats) {
		_name = name;
		_number = number;
		_description = description;
		_location = localtion;
		_seatsLeft = seats;
		_enrollmentList = new ArrayList<Student>();
	}

	public void addStudent(Student aStudent) {
		_enrollmentList.add(aStudent);
	}

	public String getName() {
		return _name;
	}
	
	public int getNumber() {
		return _number;
	}

	public String getDescription() {
		return _description;
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatsLeft() {
		return _seatsLeft;
	}

	public List<Student> getStudentList() {
		return new ArrayList<Student>(_enrollmentList);
	}
	
	public String report() {
		return report(new SeminarTextReportStrategy());
	}

	public String report(AbstractSeminarReportStrategy aSeminarReportStrategy) {
		return aSeminarReportStrategy.reportFor(this);
	}

}
