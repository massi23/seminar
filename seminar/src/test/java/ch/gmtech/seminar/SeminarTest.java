package ch.gmtech.seminar;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.gmtech.seminar.report.SeminarCSVReportStrategy;
import ch.gmtech.seminar.report.SeminarHTMLReportStrategy;

public class SeminarTest  {

	@Test
	public void testSeminarUiIntegration() throws Exception {
		Seminar seminar = new Seminar("java for dummies", 1, "learn java", "mendrisio", 23);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		assertEquals(
				"course: java for dummies - 1\n"
				+ "description: learn java\n"
				+ "location: mendrisio\n"
				+ "seats left: 23\n"
				+ "students:\n"
				+ "\tJoey Ramone"
				+ "\tDee Dee Ramone", seminar.report());
	}
	
	@Test
	public void testSeminarDetailsAsHTML() throws Exception {
		Seminar seminar = new Seminar("java for dummies", 1, "learn java", "mendrisio", 23);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		
		String expected = 
				"<html>"+
				"<head>"+
				"     <title>nome corso</title>"+
				"</head> "+
				"<body>"+
				"    <div>java for dummies</div>"+
				"    <ul>"+
				"          <li>learn java</li>"+
				"          <li>mendrisio</li>"+
				"          <li>23</li>"+
				"    </ul>"+
				"    <div>partecipanti:</div>"+
				"    <ul>"+
				"          <li>Joey Ramone</li>"+
				"          <li>Dee Dee Ramone</li>"+
				"    </ul>"+
				"</body>"+
				"</html>";
		assertEquals(expected, seminar.report(new SeminarHTMLReportStrategy()));
	}
	
	@Test
	public void testSeminarDetailsAsCSV() throws Exception {
		Seminar seminar = new Seminar("java for dummies", 1, "learn java", "mendrisio", 23);
		seminar.addStudent(new Student("Joey", "Ramone"));
		seminar.addStudent(new Student("Dee Dee", "Ramone"));
		String expected =
				"\"1\";\"java for dummies\";\"learn java\";\"mendrisio\";\"23\";\n"
				+ "\"Joey\";\"Ramone\";\n"
				+ "\"Dee Dee\";\"Ramone\";\n";
		assertEquals(expected, seminar.report(new SeminarCSVReportStrategy()));
	}
	
}
